
### ### ### ### ### ## ### ### ### ###
### ### ### BUILT-IN LIBRARIES ### ###
### ### ### ### ### ## ### ### ### ###
import logging
import cv2
import numpy as np

### ### ### ### ## ## ## ### ### ###
### ### ### CUSTOM LIBRARIES ### ###
### ### ### ### ## ## ## ### ### ###
import libs

from stdo import stdo
from tools import time_log, TIME_FLAGS, time_list, load_from_json
from qt_tools import qtimer_Create_And_Run

from structure_ui import init_and_run_UI, Graphics_View  # , Structure_UI, init_UI
from structure_camera import CAMERA_FLAGS
from structure_ui_camera import Structure_Ui_Camera
from structure_threading import Thread_Object


### ### ### ### ### ## ## ## ### ### ### ### ###
### ### ### CAMERA UI CONFIGURATIONS ### ### ###
### ### ### ### ### ## ## ## ### ### ### ### ###


class Ui_Video_Writer(Structure_Ui_Camera):
    logger_level = logging.INFO
    #__Threads = dict()
    
    def __init__(self, *args, obj=None, logger_level=logging.INFO, **kwargs):
        super(Ui_Video_Writer, self).__init__(*args, **kwargs)

        ### ### ### ### ###
        ### Constractor ###
        ### ### ### ### ###
        self.logger_level = logger_level
        self.is_Video_Saving_Stopped = True
        self.video_Reader = None

        ### ### ### ### ###
        ### ### Init ### ##
        ### ### ### ### ###
        self.init()
        
    ### ### ## ### ###
    ### OVERWRITES ###
    ### ### ## ### ###
    
    def init(self):

        self.configure_Other_Settings()
        self.connect_to_Camera(
            CAMERA_FLAGS.CV2,
            # self.spinBox_Buffer_Size.value(),
            0,
            10,
            self.exposure_Time
        )
        self.graphicsView_Camera.init_Render_QTimer(
            connector_stream=self.stream_Flow,
            delay = 0.001
        )
        # self.camera_Instance.api_Set_Camera_Size(resolution=(1920, 1080))
        
        self.graphicsView_Video.init_Render_QTimer(
            connector_stream=self.video_Flow,
            delay=0.001
        )
        self.pushButton_Video_Load.clicked.connect(
            lambda: self.video_Init(
                f"camera_{self.spinBox_Save_Video_FPS.value()}fps" + ".avi"
            )
        )
        
    def configure_Button_Connections(self):
        self.pushButton_Set_Exposure.clicked.connect(
            lambda: self.set_Camera_Exposure(
                self.spinBox_Exposure_Time.value()
            )
        )
        self.pushButton_Save_Image.clicked.connect(
            lambda: self.save_Image_Action(
                # self.camera_Instance.stream_Returner(auto_pop=False),
                img=self.api_Get_Buffered_Image(),
                path=None,
                filename=[],
                format="png"
            )
        )
        self.pushButton_Connect_to_Camera.clicked.connect(
            lambda: self.connect_to_Camera(
                CAMERA_FLAGS.CV2,
                # self.spinBox_Buffer_Size.value(),
                10,
                self.exposure_Time
            )
        )
        self.pushButton_Remove_the_Camera.clicked.connect(
            self.camera_Remove
        )
        self.pushButton_Stream_Switch.clicked.connect(
            lambda: self.stream_Switch()
        )
        self.pushButton_Save_Video.clicked.connect(
            self.video_Saving_Process
        )

    def video_Saving_Process(self):
        if self.is_Video_Save_Stop():
            self.switch_Video_Save_Stop(False)
            self.camera_Instance.save_Video_From_Buffer_Thread(
                path="",
                name=f"camera_{self.spinBox_Save_Video_FPS.value()}fps",
                extension="avi",
                fps=self.spinBox_Save_Video_FPS.value(),
                trigger_pause=self.is_Stream_Active,
                trigger_quit=self.is_Video_Save_Stop,
                number_of_snapshot=-1, 
                delay=0.001
            )
            # self.video_Init(f"camera_{self.spinBox_Save_Video_FPS.value()}fps" + ".avi")
        else:
            self.switch_Video_Save_Stop(True)
            self.video_Stop()

    def switch_Video_Save_Stop(self, bool=None):
        self.is_Video_Saving_Stopped = bool if bool is not None else not self.is_Video_Saving_Stopped
        return self.is_Video_Saving_Stopped

    def is_Video_Save_Stop(self):
        return self.is_Quit_App() if self.is_Quit_App() else self.is_Video_Saving_Stopped

    def closeEvent(self, *args, **kwargs):
        super(Ui_Video_Writer, self).closeEvent(*args, **kwargs)
        self.camera_Remove()
        
    def video_Init(self, path):
        print("video_Init:", path)
        try:
            self.video_Reader = cv2.VideoCapture(path)
        except Exception as e:
            print("video_Init Error:", e)
            
    def video_Stop(self):
        return self.video_Reader.release() if self.video_Reader is not None else None
        
    def video_Flow(self):
        frame = None
        try:
            if self.video_Reader is not None:
                is_Successfull, frame = self.video_Reader.read()
                if not is_Successfull:
                    frame = None
        except Exception as e:
            print("video_Flow Error:", e)
            self.video_Stop()
        return frame
    
    def stream_Flow(self):
        return self.camera_Instance.stream_Returner() if self.camera_Instance is not None else None
        
    def load_Video(self):
        pass
        
### ### ### ### ### ## ## ## ### ### ### ### ###
### ### ### ### ### ## ## ## ### ### ### ### ###
### ### ### ### ### ## ## ## ### ### ### ### ###

if __name__ == "__main__":
    # title, Class_UI, run=True, UI_File_Path= "test.ui", qss_File_Path = ""
    stdo(1, "Running {}...".format(__name__))
    app, ui = init_and_run_UI(
        "Video Writer",
        Ui_Video_Writer,
        UI_File_Path="video_Writer_UI.ui"
    )
